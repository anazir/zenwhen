package token

type TokenType string

type Token struct {
  Type TokenType
  Literal string
}

const (
  ILLEGAL = "ILLEGAL"
  EOF = "EOF"
  IDENT = "IDENT"
  INT = "INT"
  ASSIGN = "="
  PLUS = "+"
  COMMA = ","
  SEMICOLON = ";"
  LPAREN = "("
  RPAREN = ")"
  LBRACE = "{"
  RBRACE = "}"
  BANG = "!"
  MINUS = "-"
  ASTERISK = "*"
  SLASH = "/"
  LT = "<"
  GT = ">"
  FUNCTION = "FUNCTION"
  LET = "LET"
  IF = "IF "
  RETURN = "RETURN"
  TRUE = "TRUE"
  ELSE = "ELSE"
  FALSE = "FALSE"
  EQ = "=="
  NOT_EQ = "!="
)

var keywords = map[string]TokenType{
  "fn": FUNCTION,
  "let": LET,
  "if": IF,
  "return": RETURN,
  "true": TRUE,
  "else": ELSE,
  "false": FALSE,
}

func LookupIdent(ident string) TokenType {
  if tok, ok := keywords[ident]; ok {
    return tok
  }
  return IDENT
}
