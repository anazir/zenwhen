package main

import (
	"fmt"
	"os"
	"os/user"

	"codeberg.org/zenwhen/repl"
)

func main() {
  user, err := user.Current()
  if err != nil {
    panic(err)
  }

  fmt.Printf("Aaa aaa oooo ooo %s this is the programming language ooga booga Monke!\n", user.Username)
  fmt.Printf("Monke sound, type command\n")
  repl.Start(os.Stdin, os.Stdout)
}
